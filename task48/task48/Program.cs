﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task48
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> arr = new List<int>();
            Console.Write("Введите массив чисел через пробел: ");
            string s2 = Console.ReadLine();
            string[] mas = s2.Split(new[] { ' ' });

            for (int i = 0; i < mas.Length; i++)
            {
                if (IsDigit(mas[i]))
                    arr.Add(Convert.ToInt32(mas[i]));
            }

            List<int> arr2 = arr.Distinct().ToList();           // Удаляем дубли

            Console.WriteLine("Длина массива цифр: " + arr.Count);

            string answer = "";
            foreach (var i in arr2)
                answer += i.ToString() + " ";

            Console.WriteLine("------------------------------------");
            Console.WriteLine("Удаляем дубли из массива...");
            Console.WriteLine("Answer = " + answer + ", длина: " + arr2.Count);
            Console.ReadKey();
        }

        static bool IsDigit(string d)
        {
            if (d.Any(char.IsDigit))         // Именно str.Any, а не str.All находит цифру в строке
                return true;
            return false;
        }
    }
}
